from abc import ABC

class Animal(ABC):

	def eat(self, food):
		pass

	def make_sound(self):

		pass


class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self.name = name
		self.breed = breed
		self.age = age

	def get_name(self):
		print(f"{self.name}")

	def set_name(self, name):
		self.name = name

	def get_breed(self):
		print(f"{self.breed}")

	def set_name(self, name):
		self.breed = breed

	def get_age(self):
		print(f"{self.age}")

	def set_age(self, age):
		self.age = age

	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print("Bark! Woof! Arf!")

	def call(self):
		print(f"Here {self.name}!")


class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self.name = name
		self.breed = breed
		self.age = age

	def get_name(self):
		print(f"{self.name}")

	def set_name(self, name):
		self.name = name

	def get_breed(self):
		print(f"{self.breed}")

	def set_name(self, name):
		self.breed = breed

	def get_age(self):
		print(f"{self.age}")

	def set_age(self, age):
		self.age = age

	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaaa")

	def call(self):
		print(f"{self.name}, come on!")


dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
